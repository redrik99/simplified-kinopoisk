package com.example.simplifiedkinopoisk.repositories;

import com.example.simplifiedkinopoisk.entities.MovieEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MovieRepository extends JpaRepository<MovieEntity, Long> {

    List<MovieEntity> findByTitleContaining(String title);

    List<MovieEntity> findByPublished(boolean published);
}