package com.example.simplifiedkinopoisk.repositories;

import com.example.simplifiedkinopoisk.entities.CinemaManEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CinemaManRepository extends JpaRepository<CinemaManEntity, Long> {

    List<CinemaManEntity> findByFullNameContaining(String fullName);
}