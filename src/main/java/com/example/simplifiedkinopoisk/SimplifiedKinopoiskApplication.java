package com.example.simplifiedkinopoisk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimplifiedKinopoiskApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimplifiedKinopoiskApplication.class, args);
    }

}
