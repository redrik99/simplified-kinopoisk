package com.example.simplifiedkinopoisk.service;

import com.example.simplifiedkinopoisk.entities.CinemaManEntity;
import com.example.simplifiedkinopoisk.repositories.CinemaManRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CinemaManService {

    private final CinemaManRepository cinemaManRepository;

    public CinemaManService(CinemaManRepository cinemaManRepository) {
        this.cinemaManRepository = cinemaManRepository;
    }

    public ResponseEntity<CinemaManEntity> updateCinemaMan(long id, CinemaManEntity cinemaMan) {
        Optional<CinemaManEntity> cmData = cinemaManRepository.findById(id);

        if (cmData.isPresent()) {
            CinemaManEntity _cm = cmData.get();
            _cm.setFullName(cinemaMan.getFullName());
            return new ResponseEntity<>(cinemaManRepository.save(_cm), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    public ResponseEntity<List<CinemaManEntity>> getAll(String fullName) {
        try {
            List<CinemaManEntity> cinemaPeople = new ArrayList<>();

            if (fullName == null) {
                cinemaPeople.addAll(cinemaManRepository.findAll());
            } else {
                cinemaPeople.addAll(cinemaManRepository.findByFullNameContaining(fullName));
            }

            if (cinemaPeople.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(cinemaPeople, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<CinemaManEntity> getCinemaManById(long id) {
        Optional<CinemaManEntity> cmData = cinemaManRepository.findById(id);

        return cmData.map(cinemaMan -> new ResponseEntity<>(cinemaMan, HttpStatus.OK)).orElseGet(()
                -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
