package com.example.simplifiedkinopoisk.service;

import com.example.simplifiedkinopoisk.entities.MovieEntity;
import com.example.simplifiedkinopoisk.repositories.MovieRepository;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public ResponseEntity<List<MovieEntity>> getAllMovies(String title) {

        try {
            List<MovieEntity> movies = new ArrayList<>();

            if (title == null) {
                movies.addAll(movieRepository.findAll());
            } else {
                movies.addAll(movieRepository.findByTitleContaining(title));
            }

            if (movies.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(movies, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<MovieEntity> getMovieById(long id) {
        Optional<MovieEntity> movieData = movieRepository.findById(id);

        return movieData.map(movie -> new ResponseEntity<>(movie, HttpStatus.OK)).orElseGet(()
                -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    public ResponseEntity<MovieEntity> createMovie(MovieEntity movie) {
        try {
            movie.setRating(getRatingFromKinApi(movie.getTitle()));
            MovieEntity _movie = movieRepository.save(movie);
            return new ResponseEntity<>(_movie, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<MovieEntity> updateMovie(long id, MovieEntity movie) {
        Optional<MovieEntity> movieData = movieRepository.findById(id);

        if (movieData.isPresent()) {
            MovieEntity _movie = movieData.get();
            _movie.setTitle(movie.getTitle());
            _movie.setDescription(movie.getDescription());
            _movie.setReleaseYear(movie.getReleaseYear());
            _movie.setDirector(movie.getDirector());
            _movie.setActors(movie.getActors());
            _movie.setPublished(movie.isPublished());
            _movie.setRating(getRatingFromKinApi(movie.getTitle()));
            return new ResponseEntity<>(movieRepository.save(_movie), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<HttpStatus> deleteMovie(long id) {
        try {
            movieRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<HttpStatus> deleteAllMovies() {
        try {
            movieRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<MovieEntity>> findByPublished() {
        try {
            List<MovieEntity> movies = movieRepository.findByPublished(true);

            if (movies.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(movies, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @param keyword - for search in kinopoiskapiunofficial.tech api
     * @return - rating of movie from kinopoisk.ru via kinopoiskapiunofficial.tech
     */
    private Float getRatingFromKinApi(String keyword) {
        Float rating = null;

        try {
            URL url = new URL("https://kinopoiskapiunofficial.tech/api/v2.1/films/" +
                    "search-by-keyword?keyword=" + URLEncoder.encode(keyword, StandardCharsets.UTF_8));

            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("X-API-KEY", "6b885069-9144-441b-8c4a-d918527f8af5");

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            JSONObject jsonObject = new JSONObject(content.toString());
            String title = jsonObject.getJSONArray("films").getJSONObject(0)
                    .getString("nameRu").toLowerCase();
            if (title.equals(keyword.toLowerCase())) {
                rating = Float.parseFloat(jsonObject.getJSONArray("films").
                        getJSONObject(0).getString("rating"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rating;
    }
}
