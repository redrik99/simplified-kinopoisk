package com.example.simplifiedkinopoisk.controllers;

import com.example.simplifiedkinopoisk.entities.MovieEntity;
import com.example.simplifiedkinopoisk.service.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MovieController {

    final private MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/movies")
    public ResponseEntity<List<MovieEntity>> getAllMovies(@RequestParam(required = false) String title) {
        return movieService.getAllMovies(title);
    }

    @GetMapping("/movie/{id}")
    public ResponseEntity<MovieEntity> getMovieById(@PathVariable("id") long id) {
        return movieService.getMovieById(id);
    }

    @PostMapping("/movie")
    public ResponseEntity<MovieEntity> createMovie(@RequestBody MovieEntity movie) {
        return movieService.createMovie(movie);
    }

    @PutMapping("/movies/{id}")
    public ResponseEntity<MovieEntity> updateMovie(@PathVariable("id") long id, @RequestBody MovieEntity movie) {
        return movieService.updateMovie(id, movie);
    }

    @DeleteMapping("/movies/{id}")
    public ResponseEntity<HttpStatus> deleteMovie(@PathVariable("id") long id) {
        return movieService.deleteMovie(id);
    }

    @DeleteMapping("/movies")
    public ResponseEntity<HttpStatus> deleteAllMovies() {
        return movieService.deleteAllMovies();
    }

    @GetMapping("/movies/published")
    public ResponseEntity<List<MovieEntity>> findByPublished() {
        return movieService.findByPublished();
    }

}
