package com.example.simplifiedkinopoisk.controllers;

import com.example.simplifiedkinopoisk.entities.CinemaManEntity;
import com.example.simplifiedkinopoisk.service.CinemaManService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cinema-man")
public class CinemaPeopleController {

    final private CinemaManService cinemaManService;

    public CinemaPeopleController(CinemaManService cinemaManService) {
        this.cinemaManService = cinemaManService;
    }

    @GetMapping()
    public ResponseEntity<List<CinemaManEntity>> getAllCinemaPeople(@RequestParam(required = false) String fullName) {
        return cinemaManService.getAll(fullName);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CinemaManEntity> getCinemaManById(@PathVariable("id") long id) {
        return cinemaManService.getCinemaManById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CinemaManEntity> updateCinemaMan(@PathVariable("id") long id, @RequestBody CinemaManEntity cinemaMan) {
        return cinemaManService.updateCinemaMan(id, cinemaMan);
    }
}
