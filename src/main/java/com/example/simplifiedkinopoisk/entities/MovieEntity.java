package com.example.simplifiedkinopoisk.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@Table(name = "movies")
public class MovieEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "title", unique = true, nullable = false)
    private String title;

    @Column(name = "description", length = 2000, nullable = false)
    private String description;

    @Column(name = "release_year")
    @NumberFormat
    private int releaseYear;

    @Column(name = "rating")
    @NumberFormat
    private Float rating;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "derector_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "directorInMovies"})
    private CinemaManEntity director;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "actorInMovies"})
    private List<CinemaManEntity> actors;

    @Column(name = "published")
    private boolean published;
}
