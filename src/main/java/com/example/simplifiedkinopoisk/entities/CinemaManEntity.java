package com.example.simplifiedkinopoisk.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class CinemaManEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "full_name", unique = true, nullable = false)
    private String fullName;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "actors")
    @JsonIgnoreProperties({"description", "director", "releaseYear", "actors", "published"})
    private List<MovieEntity> actorInMovies;

    @OneToMany(mappedBy = "director", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnoreProperties({"description", "director", "releaseYear", "actors", "published"})
    private List<MovieEntity> directorInMovies;
}
