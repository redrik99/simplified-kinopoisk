package com.example.simplifiedkinopoisk.service;

import com.example.simplifiedkinopoisk.repositories.MovieRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class MovieServiceTest {

    @MockBean
    private MovieRepository mockedMovieRepository;

    @Test
    void getRatingFromKinApi() {
        MovieService ms = new MovieService(mockedMovieRepository);
        Float rating = ReflectionTestUtils.invokeMethod(ms,
                "getRatingFromKinApi", "человек-паук");
        assertEquals(rating, 7.6F);
    }
}
