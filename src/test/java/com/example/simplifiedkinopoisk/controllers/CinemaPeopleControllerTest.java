package com.example.simplifiedkinopoisk.controllers;

import com.example.simplifiedkinopoisk.entities.CinemaManEntity;
import com.example.simplifiedkinopoisk.repositories.CinemaManRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CinemaPeopleControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private CinemaManRepository mockedCinemaManRepository;


    @Test
    void getAllCinemaPeople() throws Exception {

        List<CinemaManEntity> cinemaPeople = new ArrayList<>();

        CinemaManEntity cinemaMan1 = new CinemaManEntity();
        cinemaMan1.setId(1L);
        cinemaMan1.setFullName("Full name 1");

        CinemaManEntity cinemaMan2 = new CinemaManEntity();
        cinemaMan2.setId(2L);
        cinemaMan2.setFullName("Full name 2");

        cinemaPeople.add(cinemaMan1);
        cinemaPeople.add(cinemaMan2);

        when(mockedCinemaManRepository.findAll()).thenReturn(cinemaPeople);
        this.mockMvc.perform(get("/cinema-man")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json("[{\"id\":1,\"fullName\":\"Full name 1\"" +
                        ",\"actorInMovies\":null,\"directorInMovies\":null}," +
                        "{\"id\":2,\"fullName\":\"Full name 2\",\"actorInMovies\":null,\"directorInMovies\":null}]"));

        cinemaPeople.clear();
        cinemaPeople.add(cinemaMan2);

        when(mockedCinemaManRepository.findByFullNameContaining("Full Name")).thenReturn(cinemaPeople);
        this.mockMvc.perform(get("/cinema-man/?fullName=Full Name")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json("[{\"id\":2,\"fullName\":\"Full name 2\"" +
                        ",\"actorInMovies\":null,\"directorInMovies\":null}]"));

        cinemaPeople.clear();
        when(mockedCinemaManRepository.findByFullNameContaining("no content")).thenReturn(cinemaPeople);
        this.mockMvc.perform(get("/cinema-man/?fullName=no content")).andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    void getCinemaManById() throws Exception {

        CinemaManEntity cinemaMan = new CinemaManEntity();
        cinemaMan.setId(1L);
        cinemaMan.setFullName("FullName");

        when(mockedCinemaManRepository.findById(1L)).thenReturn(Optional.of(cinemaMan));
        this.mockMvc.perform(get("/cinema-man/1")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"fullName\":\"FullName\"" +
                        ",\"actorInMovies\":null,\"directorInMovies\":null}"));

    }

    @Test
    void updateCinemaMan() throws Exception {

        CinemaManEntity cinemaMan = new CinemaManEntity();
        cinemaMan.setId(1L);
        cinemaMan.setFullName("Full Name");

        CinemaManEntity newCinemaMan = new CinemaManEntity();
        newCinemaMan.setId(1L);
        cinemaMan.setFullName("New Full Name");

        when(mockedCinemaManRepository.findById(1L)).thenReturn(Optional.of(cinemaMan));
        when(mockedCinemaManRepository.save(newCinemaMan)).thenReturn(newCinemaMan);
        this.mockMvc.perform(put("/cinema-man/1").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":1,\"fullName\":\"New Full Name\"}"))
                .andDo(print()).andExpect(status().is2xxSuccessful());
    }
}