package com.example.simplifiedkinopoisk.controllers;

import com.example.simplifiedkinopoisk.entities.MovieEntity;
import com.example.simplifiedkinopoisk.repositories.MovieRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class MovieControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private MovieRepository mockedMovieRepository;

    @Test
    void getAllMovies() throws Exception {
        List<MovieEntity> movies = new ArrayList<>();

        MovieEntity movie1 = new MovieEntity();
        movie1.setId(1L);
        movie1.setTitle("Title 1");
        movie1.setDescription("Description 1");
        movie1.setReleaseYear(2001);
        movie1.setPublished(true);

        MovieEntity movie2 = new MovieEntity();
        movie2.setId(2L);
        movie2.setTitle("Title 2");
        movie2.setDescription("Description 2");
        movie2.setReleaseYear(2002);
        movie2.setPublished(false);

        movies.add(movie1);
        movies.add(movie2);

        when(mockedMovieRepository.findAll()).thenReturn(movies);
        this.mockMvc.perform(get("/movies")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json("[{\"id\":1,\"title\":\"Title 1\",\"description\":\"Description 1\"" +
                        ",\"releaseYear\":2001,\"director\":null,\"actors\":null,\"published\":true}," +
                        "{\"id\":2,\"title\":\"Title 2\",\"description\":\"Description 2\",\"releaseYear\":2002" +
                        ",\"director\":null,\"actors\":null,\"published\":false}]"));

        movies.clear();
        movies.add(movie2);
        when(mockedMovieRepository.findByTitleContaining("Title 2")).thenReturn(movies);
        this.mockMvc.perform(get("/movies/?title=Title 2")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json("[{\"id\":2,\"title\":\"Title 2\"," +
                        "\"description\":\"Description 2\",\"releaseYear\":2002" +
                        ",\"director\":null,\"actors\":null,\"published\":false}]"));

        movies.clear();
        when(mockedMovieRepository.findByTitleContaining("no content")).thenReturn(movies);
        this.mockMvc.perform(get("/movies/?title=no content")).andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    void findById() throws Exception {
        MovieEntity movie = new MovieEntity();
        movie.setId(1L);
        movie.setTitle("Title 1");
        movie.setDescription("Description 1");
        movie.setReleaseYear(2001);
        movie.setPublished(true);

        when(mockedMovieRepository.findById(1L)).thenReturn(Optional.of(movie));
        this.mockMvc.perform(get("/movie/1")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"title\":\"Title 1\"," +
                        "\"description\":\"Description 1\",\"releaseYear\":2001" +
                        ",\"director\":null,\"actors\":null,\"published\":true}"));
    }

    @Test
    void createMovie() throws Exception {
        MovieEntity movie = new MovieEntity();
        movie.setId(1L);
        movie.setTitle("Test title");
        movie.setDescription("Test description");
        movie.setReleaseYear(2022);
        movie.setPublished(true);

        when(mockedMovieRepository.save(movie)).thenReturn(movie);
        this.mockMvc.perform(post("/movie").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":1,\"title\":\"Test title\"," +
                        "\"description\":\"Test description\",\"releaseYear\":2022" +
                        ",\"director\":null,\"actors\":null,\"published\":true}"))
                .andDo(print()).andExpect(status().is2xxSuccessful()).andExpect(content()
                .json("{\"id\":1,\"title\":\"Test title\"," +
                        "\"description\":\"Test description\",\"releaseYear\":2022" +
                        ",\"director\":null,\"actors\":null,\"published\":true}"));
    }
}
