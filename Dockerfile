FROM openjdk:17
ADD target/SimplifiedKinopoisk.jar SimplifiedKinopoisk.jar
ENTRYPOINT ["java", "-jar","SimplifiedKinopoisk.jar"]
EXPOSE 8080